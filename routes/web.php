<?php

Route::get('/', 'HomeController@index')->name('home');
Route::get('about', 'HomeController@aboutMe')->name('about');

Route::get('project-create', 'HomeController@create')->name('create');
Route::post('project-create', 'HomeController@store');

Route::get('{id}', 'HomeController@edit')->name('edit');
Route::post('{id}', 'HomeController@update');

Route::post('update-group/{id}', 'HomeController@updateGroup')->name('update-group');
Route::post('update-campaign/{id}', 'HomeController@updateCampaign')->name('update-campaign');

Route::get('delete/{id}', 'HomeController@destroy')->name('delete');
