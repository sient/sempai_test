@extends('layouts.app')

@section('content')
    <div class="col-md-12">
        <form method="POST">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="name">Nazwa projektu</label>
                <input type="text" name="name" class="form-control" id="name" placeholder="Nazwa projektu">
            </div>

            <div class="form-group">
                <label for="website">Adres strony</label>
                <input type="text" name="website" class="form-control" id="website" placeholder="Adres strony">
            </div>

            <div class="form-group">
                <label for="active">Status</label>
                <select class="form-control" name="active" id="active">
                    <option value="0">Nieaktywny</option>
                    <option value="1">Aktywny</option>
                    <option value="2">W trakcie realizacji</option>
                </select>
            </div>

            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
@endsection
