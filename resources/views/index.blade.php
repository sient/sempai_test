@extends('layouts.app')

@section('content')

    <div class="col-md-12">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <form method="GET">
                <div class="form-row">
                    <div class="col-md-10">
                        <select name="sort" id="filter" class="form-control">
                            <option value="no">Bez filtrowania</option>
                            <option {{ request('filter') == '0' ?  'selected=selected' : ''}} value="0">Nieaktywne</option>
                            <option {{ request('filter') == '1' ?  'selected=selected' : ''}} value="1">Aktywne</option>
                            <option {{ request('filter') == '2' ?  'selected=selected' : ''}} value="2">W trakcie realizacji</option>
                        </select>
                    </div>
                </div>

            </form>
        </nav>
    </div>

    @foreach($projects as $project)
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                {{ $project->name }}

                <div class="buttons">
                    <a href="{{ route('edit', ['id' => $project->id]) }}" class="btn btn-primary">Edit</a>
                    <a href="{{ route('delete', ['id' => $project->id]) }}" class="btn btn-danger">Delete</a>
                </div>
            </div>
            <div class="card-body">
                <h5 class="card-title">
                    Grupy:
                    @foreach($project->groups as $group)
                        {{ $group->name }}{{ $loop->last ? '' : ', ' }}
                    @endforeach
                </h5>
                <p class="card-text">
                    Kampanii:
                    @foreach($project->groups as $group)
                        @foreach($group->campaigns as $campaign)
                            {{ $campaign->name }}{{ $loop->last ? '' : ', ' }}
                        @endforeach
                    @endforeach
                </p>
                <a href="{{ $project->website }}" class="btn btn-primary">{{ $project->website }}</a>
            </div>
            <div class="card-footer text-muted">
                Status: {{ $project->getStatus($project->active) }}
            </div>
        </div>
    </div>
    @endforeach

    <div class="col-md-12">
        <div class="d-flex justify-content-center">
            {{ $projects->links() }}
        </div>
    </div>

@endsection

@section('js')
    <script>
        $(document).ready(() => {
            let filter = $('#filter');
            let data = null;

            filter.on('change', () => {

                if (filter.val() !== 'no') {
                    data = {
                        filter: filter.val(),
                        page: 1
                    };
                }

                sendQuery(data);
            });
        });

        function sendQuery(data) {
            $.ajax({
                type: 'GET',
                url: '{{ route('home') }}',
                dataType: 'html',
                data: data,
                success: (response) => {
                    $('body').html(response)
                }
            });
        }
    </script>
@endsection
