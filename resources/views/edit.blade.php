@extends('layouts.app')

@section('content')
    <div class="col-md-12">
        <form method="POST">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="name">Nazwa projektu</label>
                <input type="text" name="name" class="form-control" id="name" value="{{ $project->name }}" placeholder="Nazwa projektu">
            </div>

            <div class="form-group">
                <label for="website">Adres strony</label>
                <input type="text" name="website" class="form-control" id="website" value="{{ $project->website }}" placeholder="Adres strony">
            </div>

            <div class="form-group">
                <label for="active">Status</label>
                <select class="form-control" name="active" id="active">
                    <option value="0" {{ $project->active == 0 ? 'selected' : '' }}>Nieaktywny</option>
                    <option value="1" {{ $project->active == 1 ? 'selected' : '' }}>Aktywny</option>
                    <option value="2" {{ $project->active == 2 ? 'selected' : '' }}>W trakcie realizacji</option>
                </select>
            </div>

            <?php $i = 1; ?>
            @foreach($groups as $group)
                <div class="form-group" data-id="{{ $group->id }}">
                    <label>Nazwa grupy №{{ $i }}</label>
                    <input type="text" name="groups" class="form-control" value="{{ $group->name }}" placeholder="Nazwa grupy">
                </div>
                <?php $i++ ?>
            @endforeach

            @foreach($groups as $group)
                <?php $i = 1; ?>
                @foreach($group->campaigns as $campaign)

                    <div class="row" >
                        <div class="col-md-6">
                            <div class="form-group" data-id="{{ $campaign->id }}">
                                <label>Nazwa kampanii №{{ $i }}</label>
                                <input type="text" name="campaigns" class="form-control" value="{{ $campaign->name }}" placeholder="Nazwa kampanii">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group" data-id="{{ $campaign->id }}">
                                <label for="date_start">Data początkowa</label>
                                <input type="text" name="date_start" class="form-control" id="date_start" value="{{ $campaign->date_start }}" placeholder="Data początkowa">
                            </div>
                        </div>

                    </div>

                    <?php $i++ ?>
                @endforeach
            @endforeach

            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
@endsection

@section('js')
    <script>
        $(document).ready(() => {

            $('input[name=groups]').on('change', function () {
                let id = $(this).closest('div').attr('data-id');
                let href = '{{ route('update-group', ':id') }}';
                href = href.replace(':id', id);

                let data = {
                    name: $(this).val()
                };

                sendQuery(data, href);
            });

            $('input[name=campaigns]').on('change', function () {
                let id = $(this).closest('div').attr('data-id');
                let href = '{{ route('update-campaign', ':id') }}';
                href = href.replace(':id', id);

                let data = {
                    name: $(this).val()
                };

                sendQuery(data, href);
            });

            $('input[name=date_start]').on('change', function () {
                let id = $(this).closest('div').attr('data-id');
                let href = '{{ route('update-campaign', ':id') }}';
                href = href.replace(':id', id);

                let data = {
                    date_start: $(this).val()
                };

                sendQuery(data, href);
            });

            function sendQuery(data, href) {
                data['_token'] = '{{ csrf_token() }}';

                $.ajax({
                    type: 'POST',
                    url: href,
                    data: data
                });
            }

        });
    </script>
@endsection
