<?php

namespace App\Http\Controllers;

use App\Http\Requests\CampaignUpdateRequest;
use App\Http\Requests\GroupUpdateRequest;
use App\Http\Requests\ProjectsFiltersRequest;
use App\Http\Requests\ProjectRequest;
use App\Project;
use App\ProjectGroup;
use App\ProjectGroupCampaign;
use App\Services\FacebookService;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param ProjectsFiltersRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ProjectsFiltersRequest $request)
    {
        $data = $request->validated();

        if ($request->has('filter')) {
            $projects = Project::where('active', $data['filter'])->with('groups.campaigns');
        } else {
            $projects = Project::with('groups.campaigns');
        }

        return view('index')->with([
            'projects' => $projects->paginate(10)->appends(request()->except('page'))
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ProjectRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ProjectRequest $request)
    {
        $data = $request->validated();
        Project::create($data);

        return redirect()->route('home');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $project = Project::where('id', $id)->with('groups.campaigns')->firstOrFail();

        return view('edit')->with([
            'project' => $project,
            'groups'  => $project->groups
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ProjectRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(ProjectRequest $request, $id)
    {
        $data = $request->validated();
        $project = Project::where('id', $id)->firstOrFail();

        $project->update($data);

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $project = Project::where('id', $id)->firstOrFail();

        $project->delete();

        return back();
    }

    public function aboutMe()
    {
        $fb = new FacebookService();
        $token = env('FACEBOOK_APP_TOKEN');
        $response = $fb->aboutMe($token);

        dd($response->getDecodedBody());
    }

    /**
     * @param GroupUpdateRequest $request
     * @param $id
     * @return mixed
     */
    public function updateGroup(GroupUpdateRequest $request, $id)
    {
        $data = $request->validated();
        $group = ProjectGroup::where('id', $id)->first();

        if ($group)
            $group->update($data);

        return $group;
    }

    /**
     * @param CampaignUpdateRequest $request
     * @param $id
     * @return mixed
     */
    public function updateCampaign(CampaignUpdateRequest $request, $id)
    {
        $data = $request->validated();
        $campaign = ProjectGroupCampaign::where('id', $id)->first();

        if ($campaign)
            $campaign->update($data);

        return $campaign;
    }
}
