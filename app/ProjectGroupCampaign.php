<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectGroupCampaign extends Model
{
    protected $fillable = [
        'project_group_id',
        'name',
        'status',
        'date_start'
    ];

    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function group()
    {
        return $this->belongsTo(ProjectGroup::class);
    }
}
