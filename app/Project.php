<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = [
        'active',
        'website',
        'name'
    ];

    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function groups()
    {
        return $this->hasMany(ProjectGroup::class);
    }

    /**
     * @param $st
     * @return mixed
     */
    public static function getStatus($st)
    {
        $statuses = [
            0 => 'Nieaktywny',
            1 => 'Aktywny',
            2 => 'W trakcie realizacji'
        ];

        return $statuses[$st];
    }
}
