<?php

namespace App\Services;

use Facebook\Facebook;

class FacebookService
{
    protected $fb;

    /**
     * FacebookService constructor.
     */
    public function __construct()
    {
        $this->fb = $this->init();
    }

    /**
     * @return Facebook|null
     */
    protected function init()
    {
        try {
            return new Facebook([
                'app_id'                => env('FACEBOOK_APP_ID'),
                'app_secret'            => env('FACEBOOK_APP_SECRET'),
                'default_graph_version' => 'v2.10'
            ]);
        } catch (\Exception $e) {
            return null;
        }
    }

    /**
     * @param $token
     * @return \Facebook\FacebookResponse|null
     */
    public function aboutMe($token)
    {
        try {
            $response = $this->fb->get('/me', $token);
        } catch (\Exception $e) {
            return null;
        }

        return $response;
    }
}
